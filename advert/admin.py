from django.contrib import admin

from .models import Advert, Image


@admin.register(Advert)
class AdvertAdmin(admin.ModelAdmin):
    exclude = ('is_removed', 'removed_at')
    list_display = (
        'id',
        'name',
        'profile',
        'category',
        'status',
        'country',
        'city',
        'is_active',
        'sort_order',
    )
    list_filter = ('sort_order', 'is_active',)
    list_editable = ('sort_order', 'is_active',)


@admin.register(Image)
class ImageAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'is_active',
        'sort_order',
        'advert',
        'image'
    )
    list_filter = ('sort_order', 'is_active', 'advert',)
    list_editable = ('sort_order', 'is_active',)
