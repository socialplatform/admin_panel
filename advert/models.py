from functools import partial
from django.db import models
from django.utils.translation import ugettext_lazy as _

from phonenumber_field.modelfields import PhoneNumberField

from admin_panel.core.db.models import SoftTrackableModel, TrackableUpdateCreateModel
from admin_panel.core.db.managers import SoftDeletableManager
from user.models import Profile
from category.models import Category
from utils import _image_upload_to


class AbstractAdvert(SoftTrackableModel):
    objects = SoftDeletableManager()

    name = models.CharField(_('Название'), max_length=250)
    description = models.TextField(_('Описание'), max_length=2000)
    feature = models.TextField(_('Характеристики'), max_length=2000)
    status = models.CharField(_('Статус объявления'), max_length=200)
    is_active = models.BooleanField(_('Активный'), default=True)
    country = models.CharField(_('Страна'), max_length=100)
    city = models.CharField(_('Город'), max_length=100)
    address = models.TextField(_('Адрес'), max_length=500)
    phone_number = PhoneNumberField(_('Номер телефона'), blank=True, null=True)
    sort_order = models.PositiveIntegerField(_('Сортировка'), default=100)

    class Meta:
        abstract = True


class Advert(AbstractAdvert):
    profile = models.ForeignKey(Profile, verbose_name=_('Профиль'), related_name='advert', on_delete=models.CASCADE,
                                db_index=True)
    category = models.ForeignKey(Category, verbose_name=_('Категория'), related_name='advert', on_delete=models.CASCADE,
                                 db_index=True)

    def __str__(self):
        return self.name

    class Meta:
        app_label = 'advert'
        verbose_name = "Объявление"
        verbose_name_plural = "Объявления"
        ordering = ['sort_order', 'category_id', 'profile_id']


class Image(TrackableUpdateCreateModel):
    image = models.ImageField('Изображение', upload_to=partial(_image_upload_to, dirname='adverts'), default=None)
    is_active = models.BooleanField(_('Активный'), default=True)
    sort_order = models.PositiveIntegerField(_('Сортировка'), default=100)

    advert = models.ForeignKey(Advert, verbose_name=_('Объявление'), related_name='images', on_delete=models.CASCADE,
                               db_index=True)

    def __str__(self):
        return self.image.url

    class Meta:
        app_label = 'advert'
        verbose_name = "Изображение"
        verbose_name_plural = "Изображения"
        ordering = ['sort_order', 'advert_id']

