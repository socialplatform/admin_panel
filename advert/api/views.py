from django.contrib.auth import get_user_model
from django.contrib.postgres.search import SearchVector
from django_filters import rest_framework as filters

from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.pagination import LimitOffsetPagination

from admin_panel.api.renderers import APIRenderer
from advert.api.serizalizers import AdvertSerializer
from advert.models import Advert, Image as ImageModel
from category.models import Category
from user.models import Profile
from entry.models import Entry

User = get_user_model()


class AdvertsViewSet(viewsets.ModelViewSet):
    """
    Для методов retrieve, update и destroy нам помогают permission_classes
    Методы list и create переопределяем самостоятельно.
    """
    renderer_classes = [APIRenderer]
    permission_classes = [
        AllowAny
    ]
    allowed_methods = ['post', 'get', 'option', 'put']
    pagination_class = LimitOffsetPagination
    queryset = Advert.objects.filter(is_active=True).order_by('-created_at')
    serializer_class = AdvertSerializer

    filter_backends = (filters.DjangoFilterBackend,)

    def create(self, request, *args, **kwargs):
        data = request.data
        images = data.getlist('images')
        category = Category.objects.get(id=data.getlist('category')[0])
        profile = Profile.objects.get(id=data.getlist('profile')[0])

        advert = Advert(
            name=data.getlist('name')[0],
            description=data.getlist('description')[0],
            feature=data.getlist('feature')[0],
            status='На рассмотрении',
            country=data.getlist('country')[0],
            city=data.getlist('city')[0],
            address=data.getlist('address')[0],
            phone_number=data.getlist('phone_number')[0],
            category=category,
            profile=profile
        )
        advert.save()
        for im in images:
            image_object = ImageModel(image=im, advert=advert)
            image_object.save()

        entry = Entry(
            name=advert.created_at,
            description=advert.status,
            status=advert.status,
            advert=advert,
            profile=profile
        )
        entry.save()

        return Response(status=200, data={'status': 'Success'})

    def get_queryset(self):
        if self.action == 'retrieve' or self.action == 'update':
            return Advert.objects.filter(is_active=True)
        query = self.request.GET.get('query')
        profile_id = self.request.GET.get('profile_id')
        categories_list = self.request.GET.getlist('categories')

        advert_list = []

        # Объявления, созданные пользователем
        if profile_id:
            try:
                profile = Profile.objects.get(id=profile_id)
                if profile:
                    return Advert.objects.filter(profile=profile).order_by('created_at')
            except Exception:
                return []

        if query is None or query == '':
            advert_list = Advert.objects.filter(is_active=True, status='Опубликован')
        else:
            advert_list = Advert.objects.filter(is_active=True, status='Опубликован')
            advert_list = advert_list.annotate(search=SearchVector('name', 'description', 'feature')).filter(search=query)

        if not (categories_list == [] or categories_list == ['']):
            advert_list = advert_list.filter(category__in=categories_list)

        return advert_list

    def update(self, request, *args, **kwargs):
        data = request.data

        partial = kwargs.pop('partial', True)
        instance = self.get_object()
        try:
            category_object = Category.objects.get(id=data['category'])
            instance.category = category_object
            instance.status = 'На рассмотрении'

            entry = Entry.objects.get(id=instance.entry.id)
            entry.status = 'На рассмотрении'
            entry.description = 'На рассмотрении'
            entry.save()

        except Exception:
            Response('Ошибка при сохранении категории')

        serializer = self.get_serializer(instance, data=data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        entry = Entry.objects.get(id=instance.entry.id)
        entry.delete()
        self.perform_destroy(instance)
        return Response(status=204)
