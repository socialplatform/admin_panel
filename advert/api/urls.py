from django.conf.urls import url
from django.urls import include

from rest_framework import routers

from advert.api.views import AdvertsViewSet

app_name = 'advert'

router = routers.DefaultRouter()
router.register(r'adverts', AdvertsViewSet)

urlpatterns = [
    url('^', include(router.urls)),
]
