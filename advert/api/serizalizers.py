try:
    from hmac import compare_digest
except ImportError:
    def compare_digest(a, b):
        return a == b

from django.contrib.auth import get_user_model
from rest_framework import serializers

from advert.models import Advert, Image
from user.models import Profile
from category.models import Category

User = get_user_model()


class AdvertImageSerializer(serializers.HyperlinkedModelSerializer):
    original = serializers.CharField(source='image.url')

    class Meta:
        model = Image
        fields = (
            'original',
        )


class AdvertProfileSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Profile
        fields = (
            'id',
            'first_name',
            'last_name',
            'phone_number',
        )


class AdvertCategorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Category
        fields = (
            'id',
            'name',
        )


class AdvertSerializer(serializers.ModelSerializer):
    images = AdvertImageSerializer(many=True, read_only=True)
    profile = AdvertProfileSerializer(read_only=True)
    category = AdvertCategorySerializer(read_only=True)

    class Meta:
        model = Advert
        fields = (
            'id',
            'name',
            'description',
            'feature',
            'status',
            'country',
            'city',
            'address',
            'profile',
            'category',
            'created_at',
            'phone_number',
            'images',
        )
        read_only_fields = (
            'id',
            'created_at'
        )
