from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver

from utils import on_transaction_commit
from entry.models import Entry
from advert.models import Advert


@receiver(post_save, sender=Entry)
@on_transaction_commit
def post_save_advert(instance, **kwargs):
    post_save.disconnect(post_save_advert, sender=Entry)
    advert = Advert.objects.get(id=instance.advert.id)
    advert.status = instance.status
    advert.save()
    post_save.connect(post_save_advert, sender=Entry)

    # try:
    #     _check_parent_eq_self(instance)
    #     _check_parent_loops(instance)
    #     _check_nested_categories_depth(instance)
    # finally:
    #     pre_save.connect(pre_save_category, sender=Advert)

#
# @receiver(post_save, sender=Product)
# @on_transaction_commit
# def post_save_product(instance, **kwargs):
#     post_save.disconnect(post_save_product, sender=Product)
#
#     try:
#         save_all_categories_for_product(instance)
#     finally:
#         post_save.connect(post_save_product, sender=Product)
