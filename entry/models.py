from django.db import models
from django.utils.translation import ugettext_lazy as _

from admin_panel.core.db.models import SoftTrackableModel
from admin_panel.core.db.managers import SoftDeletableManager
from user.models import Profile, Moderator
from advert.models import Advert


class AbstractEntry(SoftTrackableModel):
    objects = SoftDeletableManager()

    name = models.CharField(_('Название'), max_length=250)
    status = models.CharField(_('Статус заявки'), max_length=200)
    description = models.TextField(_('Описание'), max_length=2000)
    is_active = models.BooleanField(_('Активный'), default=True)
    sort_order = models.PositiveIntegerField(_('Сортировка'), default=100)

    class Meta:
        abstract = True


class Entry(AbstractEntry):
    profile = models.ForeignKey(Profile, verbose_name=_('Профиль'), related_name='entry', on_delete=models.CASCADE,
                                db_index=True)
    advert = models.OneToOneField(Advert, verbose_name=_('Объявление'), related_name='entry', on_delete=models.CASCADE,
                                  db_index=True, blank=True, null=True)
    moderator = models.ForeignKey(Moderator, verbose_name=_('Модератор'), related_name='entry',
                                  on_delete=models.CASCADE, db_index=True, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        app_label = 'entry'
        verbose_name = "Заявка на публикацию"
        verbose_name_plural = "Заявки на публикацию"
        ordering = ['sort_order', 'advert_id', 'profile_id']

