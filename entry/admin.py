from django.contrib import admin

from .models import Entry


@admin.register(Entry)
class EntryAdmin(admin.ModelAdmin):
    exclude = ('is_removed', 'removed_at')
    list_display = (
        'name',
        'status',
        'description',
        'is_active',
        'sort_order',
        'profile',
        'advert',
        'moderator',
    )
    list_filter = ('sort_order', 'is_active',)
    list_editable = ('sort_order', 'is_active',)
