from django.apps import AppConfig


class EntryConfig(AppConfig):
    name = 'entry'
    verbose_name = "Заявки на публикацию"

    def ready(self):
        import entry.signals
