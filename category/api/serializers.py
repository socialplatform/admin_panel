try:
    from hmac import compare_digest
except ImportError:
    def compare_digest(a, b):
        return a == b

from rest_framework import serializers

from category.models import Category


class CategoryListSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Category
        fields = (
            'id',
            'name',
            'slug',
        )


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = (
            'id',
            'name',
            'slug',
            'parent_category',
        )
        read_only_fields = (
            'id'
        )
