from django.conf.urls import url
from django.urls import include

from rest_framework import routers

from category.api.views import CategoryViewSet

app_name = 'category'

router = routers.DefaultRouter()
router.register(r'categories', CategoryViewSet)

urlpatterns = [
    url('^', include(router.urls)),
]
