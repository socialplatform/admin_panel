from rest_framework import viewsets
from rest_framework.permissions import AllowAny

from admin_panel.api.renderers import APIRenderer
from category.api.serializers import CategoryListSerializer
from category.models import Category


class CategoryViewSet(viewsets.ModelViewSet):
    """
    Для методов retrieve, update и destroy нам помогают permission_classes
    Методы list и create переопределяем самостоятельно.
    """
    renderer_classes = [APIRenderer]
    permission_classes = [
        AllowAny
    ]
    allowed_methods = ['get', 'option']
    queryset = Category.objects.all().order_by('name')
    serializer_class = CategoryListSerializer
