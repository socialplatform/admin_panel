from django.db import models
from django.utils.translation import ugettext_lazy as _

from admin_panel.core.db.models import TrackableSoftDeletableModel
from admin_panel.core.db.managers import SoftDeletableManager


class AbstractCategory(TrackableSoftDeletableModel):
    objects = SoftDeletableManager()

    name = models.CharField(_('Название'), max_length=255)
    slug = models.SlugField(_('Slug'), max_length=255, unique=True, db_index=True)
    parent_category = models.ForeignKey('self', null=True, blank=True,
                                        verbose_name=_('Родительская категория'), on_delete=models.CASCADE)
    sort_order = models.PositiveIntegerField(_('Sort order'), default=100)
    is_active = models.BooleanField(_('Активный'), default=True)

    def __str__(self):
        return self.name

    class Meta:
        abstract = True


class Category(AbstractCategory):
    class Meta:
        app_label = 'category'
        verbose_name = "Категория"
        verbose_name_plural = "Категории"
        ordering = ['-parent_category_id', 'sort_order', 'name']
