from django.contrib import admin

from category.models import Category


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    exclude = ('is_removed', 'removed_at')
    list_display = (
        'name',
        'slug',
        'parent_category',
        'sort_order',
        'is_active',
    )
    list_filter = ('is_active', 'parent_category')
    list_editable = ('sort_order', 'is_active')
