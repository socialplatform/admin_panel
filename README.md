# Start project `admin_panel`

Before start project we must run `Docker` project `https://gitlab.com/socialplatform/docker`

1. `pyenv local python3.7`. Before `pyenv install python 3.7`. Docs in `https://github.com/pyenv/pyenv`
1. `pipenv shell`
2. `pip install -r requirements.txt`
3. `python manage.py makemigrations`
4. `python manage.py migrate`
5. `python manage.py createsuperuser`
6. `python manage.py runserver`