from django.urls import path, include

urlpatterns = [
    # auth API
    path('auth/', include('user.api.urls', namespace='auth')),
    path('advert/', include('advert.api.urls', namespace='advert')),
    path('category/', include('category.api.urls', namespace='category')),
]
