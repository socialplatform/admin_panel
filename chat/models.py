from django.db import models
from django.utils.translation import ugettext_lazy as _

from admin_panel.core.db.models import SoftTrackableModel
from admin_panel.core.db.managers import SoftDeletableManager
from user.models import Profile


class AbstractChat(SoftTrackableModel):
    objects = SoftDeletableManager()

    is_active = models.BooleanField(_('Активный'), default=True)
    sort_order = models.PositiveIntegerField(_('Сортировка'), default=100)

    class Meta:
        abstract = True


class Chat(AbstractChat):
    users = models.ManyToManyField(
        Profile, verbose_name=_('Пользователи'),
        related_name='chats',
        related_query_name='chat'
    )

    def __str__(self):
        return f'{self.id}'

    class Meta:
        app_label = 'chat'
        verbose_name = "Чат"
        verbose_name_plural = "Чаты"
        ordering = ['sort_order', '-created_at']


class AbstractMessage(SoftTrackableModel):
    objects = SoftDeletableManager()

    is_active = models.BooleanField(_('Активный'), default=True)

    class Meta:
        abstract = True


class Message(AbstractMessage):
    author = models.ForeignKey(Profile, verbose_name=_('Автор'), related_name='author',
                               on_delete=models.CASCADE, db_index=True)
    text = models.TextField(_('Текст'), max_length=1500)
    chat = models.ForeignKey(Chat, verbose_name=_('Чат'), related_name='messages', on_delete=models.CASCADE,
                             db_index=True)

    def __str__(self):
        return f'{str(self.created_at)[:19]} ({self.author})'

    class Meta:
        app_label = 'chat'
        verbose_name = "Сообщение"
        verbose_name_plural = "Сообщения"
        ordering = ['-created_at', 'author_id']
