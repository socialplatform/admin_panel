from django.contrib import admin

from .models import Chat, Message


@admin.register(Chat)
class ChatAdmin(admin.ModelAdmin):
    exclude = ('is_removed', 'removed_at')
    list_display = (
        'id',
        'is_active',
        'sort_order',
    )
    list_editable = ('sort_order', 'is_active',)
    list_filter = ('is_active', 'users',)


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    exclude = ('is_removed', 'removed_at')
    list_display = (
        'is_active',
        'author',
        'text',
        'chat',
    )
    list_display_links = ('chat',)
    list_filter = ('is_active', 'author', 'chat',)
    list_editable = ('is_active',)
