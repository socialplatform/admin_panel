import uuid

from django.db import transaction


def _image_upload_to(instance, filename, dirname):
    return '{0}/{1}/{2}/{3}{4}'.format(dirname, str(uuid.uuid4())[:2], str(uuid.uuid4())[:2],
                                       str(uuid.uuid4())[:10], filename[-4:])


def on_transaction_commit(func):
    def inner(*args, **kwargs):
        transaction.on_commit(lambda: func(*args, **kwargs))

    return inner