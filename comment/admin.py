from django.contrib import admin

from .models import Comment, Review, Complaint


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    exclude = ('is_removed', 'removed_at')
    list_display = (
        'name',
        'is_active',
        'sort_order',
        'author',
        'text',
        'advert',
    )
    list_filter = ('sort_order', 'is_active', 'author', 'advert',)
    list_editable = ('sort_order', 'is_active',)


@admin.register(Review)
class ReviewAdmin(admin.ModelAdmin):
    exclude = ('is_removed', 'removed_at')
    list_display = (
        'name',
        'is_active',
        'sort_order',
        'author',
        'text',
        'profile',
    )
    list_filter = ('sort_order', 'is_active', 'author', 'profile',)
    list_editable = ('sort_order', 'is_active',)


@admin.register(Complaint)
class ComplaintAdmin(admin.ModelAdmin):
    exclude = ('is_removed', 'removed_at')
    list_display = (
        'name',
        'is_active',
        'sort_order',
        'author',
        'title',
        'description',
        'profile',
        'advert',
    )
    list_filter = ('sort_order', 'is_active', 'author', 'profile', 'advert',)
    list_editable = ('sort_order', 'is_active',)
