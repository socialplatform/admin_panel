from django.db import models
from django.utils.translation import ugettext_lazy as _

from admin_panel.core.db.models import SoftTrackableModel
from admin_panel.core.db.managers import SoftDeletableManager
from user.models import Profile
from advert.models import Advert


class AbstractComment(SoftTrackableModel):
    objects = SoftDeletableManager()

    name = models.CharField(_('Название'), max_length=250)
    is_active = models.BooleanField(_('Активный'), default=True)
    sort_order = models.PositiveIntegerField(_('Сортировка'), default=100)

    class Meta:
        abstract = True


class Comment(AbstractComment):
    author = models.ForeignKey(Profile, verbose_name=_('Автор'), related_name='comments', on_delete=models.CASCADE,
                               db_index=True)
    text = models.TextField(_('Текст'), max_length=1500)
    advert = models.ForeignKey(Advert, verbose_name=_('Объявление'), related_name='comments', on_delete=models.CASCADE,
                               db_index=True)

    def __str__(self):
        return f'{self.name} ({self.author})'

    class Meta:
        app_label = 'comment'
        verbose_name = "Комментарий"
        verbose_name_plural = 'Комментарии'
        ordering = ['sort_order', 'name', 'advert_id', 'author_id']


class Review(AbstractComment):
    author = models.ForeignKey(Profile, verbose_name=_('Автор'), related_name='author_review',
                               on_delete=models.CASCADE, db_index=True)
    text = models.TextField(_('Текст'), max_length=1500)
    profile = models.ForeignKey(Profile, verbose_name=_('Пользователь'), related_name='reviews', on_delete=models.CASCADE,
                                db_index=True)

    def __str__(self):
        return f'{self.name} ({self.author})'

    class Meta:
        app_label = 'comment'
        verbose_name = "Отзыв"
        verbose_name_plural = 'Отзывы'
        ordering = ['sort_order', 'name', 'profile_id', 'author_id']


class Complaint(AbstractComment):
    author = models.ForeignKey(Profile, verbose_name=_('Автор'), related_name='author_complaint',
                               on_delete=models.CASCADE, db_index=True)
    title = models.CharField(_('Заголовок'), max_length=250)
    description = models.TextField(_('Текст'), max_length=1500)
    profile = models.ForeignKey(Profile, verbose_name=_('Пользователь'), related_name='complaints', on_delete=models.CASCADE,
                                db_index=True)
    advert = models.ForeignKey(Advert, verbose_name=_('Объявление'), related_name='complaints', on_delete=models.CASCADE,
                               db_index=True)

    def __str__(self):
        return f'{self.name} ({self.author})'

    class Meta:
        app_label = 'comment'
        verbose_name = "Жалоба"
        verbose_name_plural = 'Жалобы'
        ordering = ['sort_order', 'name', 'advert_id', 'profile_id', 'author_id']

