import logging

from django.contrib.auth import login, get_user_model
from django.contrib.auth.signals import user_logged_in
from django.db import transaction
from knox.views import LoginView as KnoxLoginView
from rest_framework import permissions, serializers
from rest_framework.exceptions import APIException
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView

from admin_panel.api.renderers import APIRenderer
from user.api.serializers import RegisterSerializer, ProfileSerializer, LoginSerializer, LoginedProfileSerializer
from user.models import Profile
from user.utils import generate_token

User = get_user_model()


def _user_to_json(user: User) -> dict:
    return {
        'id': user.id,
        'email': user.email
    }


class RegistrationView(APIView):
    permission_classes = (permissions.AllowAny,)
    renderer_classes = [APIRenderer]

    def post(self, request: Request):
        data = request.data
        data['username'] = data['email']

        serializer = RegisterSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        logging_context = {
            'data': serializer.validated_data,
            'email': serializer.validated_data['email'],
        }

        with transaction.atomic():
            user = serializer.save()
            if not user:
                error_message = 'Ошибка при регистрации пользователя. Пользователь не создан.'
                logging.error(error_message, logging_context)

                raise APIException({'common': 'Ошибка, попробуйте еще раз.'})

            if user:
                data['user'] = user.id
                profile_serializer = ProfileSerializer(data=data)
                profile_serializer.is_valid(raise_exception=True)

                phone_number = profile_serializer.validated_data['phone_number']

                if Profile.objects.filter(phone_number=phone_number).exists():
                    logging.warning(f'Введенный номер {phone_number} уже используется.', logging_context)
                    raise serializers.ValidationError(detail={'phone_number': 'Введенный номер уже используется.'})
                else:
                    profile = profile_serializer.save()
                    if not profile:
                        error_message = 'Ошибка при создании профиля пользователя. Профиль не создан.'
                        logging.error(error_message, logging_context)

                        raise APIException({'common': 'Ошибка, попробуйте еще раз.'})

        logging.info(f'Пользователь {logging_context["email"]} создан.', logging_context)
        return Response({
            'user': _user_to_json(user),
        })


class LoginView(KnoxLoginView):
    permission_classes = (permissions.AllowAny,)
    renderer_classes = [APIRenderer]

    def post(self, request: Request, **kwargs):
        serializer = LoginSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        user = serializer.validated_data['user']
        login(request, user)
        user_logged_in.send(sender=request.user.__class__,
                            request=request, user=request.user)

        data = generate_token(user)
        profile = Profile.objects.filter(user=user.id).first()
        profile_data = LoginedProfileSerializer(profile)

        data.update({
            'profile': profile_data.data,
            'user': _user_to_json(user),
        })
        return Response(data)
