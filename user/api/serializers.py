from user.models import Profile

try:
    from hmac import compare_digest
except ImportError:
    def compare_digest(a, b):
        return a == b

from django.contrib.auth import get_user_model, authenticate
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers
from rest_framework.validators import UniqueValidator

import phonenumbers

User = get_user_model()


def validate_phone_serializer(serializer, value):
    """Валидируем номер и приводим к типу PhoneNumber из phonenumbers"""
    try:
        phone = phonenumbers.parse(value)
        if not phonenumbers.is_possible_number(phone) or not phonenumbers.is_valid_number(phone):
            raise serializers.ValidationError(detail={
                'phone_number': 'Неправильный формат номера телефона.'
            })
    except phonenumbers.NumberParseException:
        raise serializers.ValidationError(detail={
            'phone_number': 'Неправильный формат номера телефона.'
        })

    return phone


def get_phone_number_field(validators=None):
    return serializers.CharField(
        min_length=12,
        max_length=18,
        required=True,
        validators=validators,
        error_messages={
            'invalid': _('Неправильный формат номера телефона.'),
            'blank': _('Номер не может быть пустым.'),
            'required': _('Номер должен быть указан.'),
            'max_length': _('Номер должен содержать менее {max_length} символов.'),
            'min_length': _('Номер должен содержать как минимум {min_length} символов.'),
        }
    )


def get_email_field(validators=None):
    return serializers.EmailField(
        required=True,
        validators=validators,
        error_messages={
            'invalid': _('Указан некорректный e-mail.'),
            'blank': _('Email не может быть пустым.'),
            'required': _('Значение должно быть указано.'),
        }
    )


def get_password_field():
    return serializers.CharField(
        min_length=6,
        max_length=100,
        write_only=True,
        required=True,
        trim_whitespace=False,
        error_messages={
            'invalid': _('Значение не корректно.'),
            'blank': _('Пароль не может быть пустым.'),
            'required': _('Значение должно быть указано.'),
            'max_length': _('Пароль должен содержать менее {max_length} символов.'),
            'min_length': _('Пароль должен содержать как минимум {min_length} символов.'),
        }
    )


def get_name_field():
    return serializers.CharField(
        min_length=1,
        max_length=100,
        write_only=True,
        required=True,
        trim_whitespace=False,
        error_messages={
            'invalid': _('Значение не корректно.'),
            'blank': _('Пароль не может быть пустым.'),
            'required': _('Значение должно быть указано.'),
            'max_length': _('Пароль должен содержать менее {max_length} символов.'),
            'min_length': _('Пароль должен содержать как минимум {min_length} символов.'),
        }
    )


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            'id',
            'username',
            'first_name',
            'last_name',
            'email',
        ]


class LoginedProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = [
            'id',
            'first_name',
            'last_name',
            'phone_number'
        ]


class ProfileSerializer(serializers.ModelSerializer):
    user = serializers.IntegerField()

    first_name = get_name_field()
    last_name = get_name_field()
    phone_number = get_phone_number_field()

    monitoring_notify = serializers.BooleanField(
        required=False,
        error_messages={
            'invalid': _('Значение не корректно'),
        }
    )

    def create(self, validated_data: dict) -> Profile:
        user = User.objects.get(id=validated_data['user'])
        profile = Profile(user=user, first_name=validated_data['first_name'],
                          last_name=validated_data['last_name'], phone_number=validated_data['phone_number'],
                          monitoring_notify=validated_data['monitoring_notify'])
        profile.save()
        return profile

    def validate(self, attrs):
        if not attrs['phone_number']:
            raise serializers.ValidationError(detail={
                'phone_number': 'Номер должен быть указан.'
            })

        validate_phone_serializer(serializers, attrs['phone_number'])

        return attrs

    class Meta:
        model = Profile
        fields = [
            'user', 'phone_number', 'monitoring_notify', 'first_name', 'last_name',
        ]


class RegisterSerializer(serializers.ModelSerializer):
    email = get_email_field(validators=[UniqueValidator(queryset=User.objects.all(), message='Введенный email уже используется.')])

    password = get_password_field()
    passwordrep = get_password_field()
    check = serializers.BooleanField(
        required=True,
        error_messages={
            'invalid': _('Значение не корректно'),
            'required': _('Значение должно быть указано.'),
        }
    )
    first_name = get_name_field()
    last_name = get_name_field()

    def create(self, validated_data: dict) -> User:
        user = User(email=validated_data['email'], username=validated_data['email'],
                    is_active=True, first_name=validated_data['first_name'], last_name=validated_data['last_name'])
        user.set_password(validated_data['password'])
        user.save()
        return user

    def validate(self, attrs):
        if not attrs['check']:
            raise serializers.ValidationError(detail={
                'check': 'Требуется согласие с Политикой конфиденциальности'
            })

        if attrs['password'] != attrs['passwordrep']:
            raise serializers.ValidationError(detail={
                'password': 'Введенные пароли не совпадают',
                'passwordrep': 'Введенные пароли не совпадают',
            })

        return attrs

    class Meta:
        model = User
        fields = ('id', 'email', 'password', 'passwordrep', 'check', 'first_name', 'last_name')


class LoginSerializer(serializers.Serializer):
    email = get_email_field()
    password = get_password_field()

    def validate(self, attrs):
        user = User.objects.filter(email=attrs.get('email')).first()
        if not user:
            raise serializers.ValidationError(detail={
                'email': True,
                'password': True,
                'common': 'Введенная пара email-пароль не найдена!'
            })

        if not user.is_active:
            raise serializers.ValidationError(detail={
                'email': 'Email пользователя не подтвержден.'
            })

        username = user.username
        password = attrs['password']
        user = authenticate(request=self.context.get('request'),
                            username=username,
                            password=password)

        # The authenticate call simply returns None for is_active=False
        # users. (Assuming the default ModelBackend authentication
        # backend.)
        if not user:
            raise serializers.ValidationError(detail={
                'email': True,
                'password': True,
                'common': 'Введенная пара email-пароль не найдена!'
            })

        attrs['user'] = user
        return attrs
