from django.contrib import admin

from .models import Profile, Moderator, Administrator


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    readonly_fields = ('created_at', 'updated_at')
    list_display = ('user', 'phone_number', 'monitoring_notify', 'created_at', 'is_activate',)
    list_editable = ('monitoring_notify',)


@admin.register(Moderator)
class ModeratorAdmin(admin.ModelAdmin):
    readonly_fields = ('created_at', 'updated_at')
    list_display = ('user', 'phone_number', 'monitoring_notify', 'created_at', 'is_activate',)
    list_editable = ('monitoring_notify',)


@admin.register(Administrator)
class AdministratorAdmin(admin.ModelAdmin):
    readonly_fields = ('created_at', 'updated_at')
    list_display = ('user', 'phone_number', 'monitoring_notify', 'created_at', 'is_activate',)
    list_editable = ('monitoring_notify',)
