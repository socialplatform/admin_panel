from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import ugettext_lazy as _

from phonenumber_field.modelfields import PhoneNumberField

from admin_panel.core.db.models import SoftTrackableModel

User = get_user_model()


class AbstractUser(SoftTrackableModel):
    phone_number = PhoneNumberField(_('Номер телефона'), blank=True, null=True)
    first_name = models.CharField(_('Имя'), max_length=255, default='')
    last_name = models.CharField(_('Фамилия'), max_length=255, default='')
    monitoring_notify = models.BooleanField(_('Уведомления'), default=False)
    is_activate = models.BooleanField(_('Активный'), default=False)

    class Meta:
        abstract = True


class Profile(AbstractUser):
    user = models.OneToOneField(User, verbose_name=_('Пользователь'), related_name='profile', on_delete=models.CASCADE, db_index=True)

    def __str__(self):
        return str(self.user)

    class Meta:
        verbose_name = _('Профиль')
        verbose_name_plural = _('Профиль')
        app_label = 'user'
        ordering = ['-created_at', 'user__username']


class Moderator(AbstractUser):
    user = models.OneToOneField(User, verbose_name=_('Пользователь'), related_name='moderator', on_delete=models.CASCADE, db_index=True)

    def __str__(self):
        return str(self.user)

    class Meta:
        verbose_name = _('Модератор')
        verbose_name_plural = _('Модераторы')
        app_label = 'user'
        ordering = ['-created_at', 'user__username']


class Administrator(AbstractUser):
    user = models.OneToOneField(User, verbose_name=_('Пользователь'), related_name='admin', on_delete=models.CASCADE, db_index=True)

    def __str__(self):
        return str(self.user)

    class Meta:
        verbose_name = _('Администратор')
        verbose_name_plural = _('Администраторы')
        app_label = 'user'
        ordering = ['-created_at', 'user__username']
